# Copyright (c) 2015 Art & Logic client, Inc. All Rights Reserved.


def Decode(value):
   hex_value = '0x' + str(value)
   int_value = int(hex_value, 16)
    
   high_order = (int_value >> 8) << 7
   low_order = int_value & 0b0000000011111111

   return int(high_order | low_order) - 8192


class Parser:
   fPenState = 0
   fOutOfBounds = False
   fX = 0
   fY = 0
   fRecursionDepth = 0

   def Parse(self, command):
      self.fPenState = 0
      self.fOutOfBounds = False
      self.fX = 0
      self.fY = 0
      self.fRecursionDepth = 0
      
      return self._Parse(command)
      
   def _Parse(self, command):
      '''
      Recursively parse a hexadecimal string containing opcodes and remainder/data and 
      produce an output of human-readable commands that control the motions 
      of a colored drawing pen.
      '''
      opcode = command[0:2]
      remainder = command[2:]
       
      if "" == opcode:        # terminate recursion
         return ""
      
      elif "F0" == opcode:    # clear all
         return "CLR;\n" + self._Parse(remainder)
         
      elif "80" == opcode:    # pen up or down
         direction = "DOWN"
         self.fPenState = 1
         if 0 == Decode(remainder[0:4]):
            direction = "UP"
            self.fPenState = 0
         return "PEN " + direction + ";\n" +  self._Parse(remainder[4:])
         
      elif "A0" == opcode:    # set pen color
         return ("C0 " + 
               str(Decode(remainder[0:4])) + " " +   
               str(Decode(remainder[4:8])) + " " +   
               str(Decode(remainder[8:12])) + " " +  
               str(Decode(remainder[12:16])) + ";\n" + 
               self._Parse(remainder[16:])
         )
         
      elif "C0" == opcode:    # move pen to position
         self.fRecursionDepth = 0
         return self._ParseMove(remainder) + "\n" + self._Parse(remainder[self.fRecursionDepth * 8:])
            
   def _ParseMove(self, remainder):
      '''
      Recursively parse the hexadecimal string looking for additional relative coordinate moves until
      a different opcode is encountered.
      '''
      possibleOpcode = remainder[0:2]
      
      if possibleOpcode == "F0" or possibleOpcode == "80" or possibleOpcode == "A0" or possibleOpcode == "":
         return ""
      else:
#          self.fX = self.fX + Decode(remainder[0:4])
#          self.fY = self.fY + Decode(remainder[4:8])
         
         deltaX = Decode(remainder[0:4])
         deltaY = Decode(remainder[4:8])
         
         self.fRecursionDepth += 1
         
         if (self.fX + deltaX > 8191 or self.fX + deltaX < -8191 or self.fY + deltaY > 8191 or self.fY + deltaY < -8191) and not(self.fOutOfBounds):
            self.fOutOfBounds = True
            self.fPenState = 0
            if 8191 - self.fX < 0:
               toX = "8191"
            elif -8191 - self.fX > 0:
               toX = "-8191"
            else:
               toX = str(self.fX + deltaX)
               
            self.fX += deltaX
                
            if 8191 - self.fY < 0:
               toY = "8191"
            elif -8191 - self.fY > 0:
               toY = "-8191"
            else:
               toY = str(self.fY + deltaY)
               
            self.fY += deltaY
       
            return "\nMV (" + toX + ", " + toY + ");\nPEN UP\n" + self._ParseMove(remainder[8:])
         elif self.fX + deltaX <= 8191 and self.fX + deltaX >= -8191 and self.fY + deltaY <= 8191 and self.fY + deltaY >= -8191 and self.fOutOfBounds:
            self.fOutOfBounds = False
            self.fPenState = 1
             
            if 8191 - self.fX < 0:
               toX = "8191"
            elif -8191 - self.fX > 0:
               toX = "-8191"
            else:
               toX = str(self.fX + deltaX)
               
            self.fX += deltaX
                
            if 8191 - self.fY < 0:
               toY = "8191"
            elif -8191 - self.fY > 0:
               toY = "-8191"
            else:
               toY = str(self.fY + deltaY)
               
            self.fY += deltaY
       
            return "\nMV (" + toX + ", " + toY + ");\nPEN DOWN\n" + self._ParseMove(remainder[8:])
         else:
            self.fX += deltaX
            self.fY += deltaY
            result = ""
            if self.fRecursionDepth == 1:
               result = "MV "
            return result + "(" + str(self.fX) + ", " + str(self.fY) + ") " + self._ParseMove(remainder[8:])

            
         
if __name__ == '__main__':
   parser = Parser()
#    print(parser.Parse("C040104010"))
#    print(parser.Parse("C040004000804001C05F205F20804000"))
   print(parser.Parse("F0A04000417F4000417FC040004000804001C05F205F20804000"))
   print(parser.Parse("F0A040004000417F417FC040004000804001C05F204000400001400140400040007E405B2C4000804000"))
   print(parser.Parse("F0A0417F40004000417FC067086708804001C0670840004000187818784000804000"))
#    print(parser.Parse("F0A0417F41004000417FC067086708804001C067082C3C18782C3C804000"))
   
#    assert(_Parse("F0A04000417F4000417FC040004000804001C05F205F20804000"), "hello")